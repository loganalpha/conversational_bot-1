# conversational_bot
This is the core backend server that handles interactions between the user and the RASA backend.
This documentation assumes that the user has RASA server running 
(either locally or one can use the AWS server). To get the `conversational_bot` run locally, use 
the following [chatbot_creator](https://bitbucket.org/aiirteam/recreator/src/master/).

## PYTHON CONSUMER IMPORTS
- the following imports are needed if you want to follow examples of API's in python. 
For other consumers (like a `javascript`, `curl` etc use appropriate functionalities, 
but the `API` logic is the same).

```Python
import json
import requests
```

## SERVER PATHS
- the address of local RASA endpoint is:
```Python
server =  'http://localhost:5005/webhooks/rest/webhook'
```

- the address of AWS RASA endpoint:
```Python
server =  'http://ec2-3-237-38-35.compute-1.amazonaws.com:5005/webhooks/rest/webhook'
```

# (1) USER TO CHATBOT APIS

### (1.1) API: USER SENDS TEXT//MESSAGE TO CHATBOT
- whenever you want to send a text message to the chatbot you must do the following:
```Python
response = requests.post(server, json={'sender': '1024', 'message': 'hi'})
``` 

- where the json is the `dictionary` with `string` keys: `sender` and `message`
    - `sender` value is a `string` representing `UNIQUE` identifier of a sender; 
    in our case it was `1024`
    - `message` value is a string representing the `text` to be 
    sent && interpreted by the chatbot. In our example it was `hi`

- in Python, the decoding could be done via:
```Python
result = json.loads(response.content)
```

- what is the structure of result depends on the `text`sent but also on the 
state in which the `chatbot` is. We described all possibilities in this
 [README](https://bitbucket.org/aiirteam/recreator/src/master/README.md)

- in our case the result will be:
```Python
[{'recipient_id': '1024', 
'text': 'Hey, I am always here to help you with your technical difficulties.\n Try to describe the problem in short descriptive way: `I might have computer virus`'}
]
```

### (1.2) API: USER SENDS AN INTENT TO THE CHATBOT
- `intent` is a special fixed string that might be sent to the endpoint. This is typically sent as 
a part of button payload (see the button section of this `README` file). 

- An example `intent` is "/conversation_restart". This intent in particular ends and restarts the 
conversation at any point.

- There are more intents, but again the precise forms of intents that need to be sent are 
typically parts of button payloads.

- Sending the `intent` is the same as sending the text message except it must be escaped, 
see the next example: 

```Python
response = requests.post(server, json={'sender': '1024', 'message': '/conversation_restart'})
```

### (1.3) API: USER SENDS LOGS AND LOGGING INTENT TO THE CHATBOT
- this is very special intent, that serves purely to send logs to the endpoint and so store the local logs
(whatever they might be: like the scripts that were used, local system etc..) to the backend, so the backend will
store them as a part of the tracker store.

- Sending the `logging intent` is very similar to sending the normal intent back to backend, except the logs must be 
converted to string and send as follows:

- if the log we want to send is FOR EXAMPLE following (the structure might be different):
```Python
log_we_want_to_save = {'system_type': 'windows_xp', 'scripts_run': ['printer_issue.ps', 'vpn.ps'], ...}

```

- then we need to first convert the log message into the string representation; i.e. to do the following:
(again I am showing Python version, but you should do the same in whatever language you are using for local agent)
```Python
log_we_want_to_save_STRING = "{'system_type': 'windows_xp', 'scripts_run': ['printer_issue.ps', 'vpn.ps'], ...}"
```

- then the logs are sent in the following manner:

```Python
response = requests.post(server, json={'sender': '1024', 'message': '/log_message{"local_logs": """{"system_type": "windows_xp", "scripts_run": ["printer_issue.ps", "vpn.ps"], ...}"""}'})
```

- EXTREMELY IMPORTANT:
    - the logs should be sent in the above format ONLY if the local agent receives message:
        - `"<RETRIEVE_LOGS>"`
        - this message MUST NOT be displayed to the user, its just a signal to the local agent that the logs
        should be created and sent back to the endpoint in above described fashion.
        
- NOTE:
    - in the above `requests.post ... ` the value under key `message` must be a string.
    - the `/log_message` intent is the same intent style as we had above, it is just the escaped string, 
    signifying that it is an intent string.
    - now, the real difference is, that after the escaped string, there are curly brackets: `/log_message{...}`
    - the curly brackets symbolize something the RASA calls entities.
    - so the message: `/log_message{"local_logs": "..."}` we are sending the entity called `local_logs`
    - the value under `local_logs` MUST BE a string, that string is representing the logs, you want to send to the 
    backend, so the backend will log them on server, for the current user and current session.
    - extremely important, the logs, should be sent ONLY AFTER local agent receives special message: `"<RETRIEVE_LOGS>"`

- TO RECAP:
    - the overall pipeline of logs handling is simple but lets recap it:
        - (0) chatbot works normally, sending messages back and forth
        - (1) at some point your local agent will get SPECIAL message `"<RETRIEVE_LOGS>"`
        - (2) in that case the local agent WILL NOT display that message, but rather it will grab the current local 
        logs and will send them back to backend in the following json:
        - `{'sender':'1024', 'message': '/log_message{"local_logs": "< ... LOCAL LOGS CONVERTED TO STRING ...>"}'}`
        - (4) the local bot continues normally as before. 

## (2) CHATBOT TO USER APIS

### (2.1) CHATBOT SENDS REPLY BACK TO THE USER; GENERAL STRUCTURE
- The chatbot always sends back the lists of dictionaries (see examples above).

- Some example of received list of dictionaries:


Empty list
```Python
[]
```

List with one dictionary
```Python
[{'recipient_id': '1024', 
'text': 'Hey, I am always here to help you with your technical difficulties...'}
]
```

List with multiple dictionaries:
```Python
[{'recipient_id': '42', 'text': "Running script: ('IpConfigRenew', 0.9686018228530884)"}, 
{'recipient_id': '42', 
'custom': {'scrips_and_probabilities': ['IpConfigRenew', 0.9686018228530884]}}, 
{'recipient_id': '42', 'text': 'Hey, did it worked out :) ?'}]
```

- NOTE, the order of the dictionaries IS ALWAYS the order in which the chatbot wants to show//execute the 
dictionaries.

- The structure of concrete dictionaries with the concrete keys and their interpretations are described below.


### (2.1) SPECIAL CASE: EMPTY LIST
- if empty list is received, this generally should never happen. 
- But if it does this is a signal that endpoint is listening for a new message from the user. 
- So in that case no action should be taken and the local chat windows should be in the listening mode.

### (2.2) DICTIONARY HOLDING ONLY TEXT

- The example of the `dictionary holding text//message`: 

```Python
{'recipient_id': '1024', 
'text': 'Hey, I am always here to help you with your technical difficulties.\n Try to describe the problem in short descriptive way: `I might have computer virus`'}
```

- This is a dictionary that holds the text message the chatbot wants to display to the user. 

- The dictionary has the following structure:
    - `recipient_id` is a `string` representing the `UNIQUE` identifier of a sender. And is the `SAME` as 
    the value of `sender`. This is weird that they have different names, but it is how `RASA` designed it.
    
    - `text`: is a `string` representing the chatbot reply. This is the message the chatbot wants to show to 
    the user with `id` =  `1024`.
    
- The local implementation should show the text message held under `text` in above dictionary. Also, 
the message should be split on "\n" (newline) and particular text blog written in a `new line`.
            
### (2.3) DICTIONARY HOLDING THE BUTTONS

- The example of the dictionary holding button is:
```Python
{'recipient_id': '42', 
'text': "Sorry. I'm not sure I've understood you correctly. Can you pick some of the following options", 
'buttons': [{'title': 'Yes!', 'payload': '/affirm{}'}, 
            {'title': 'Something else, contact human help.', 
            'payload': '/contact_human{}'}]
}
```
- This is a dictionary that holds the buttons and some extra data the chatbot wants to display to the user.

- The dictionary has the following structure:
    - `recipient_id` is a `string` representing the `UNIQUE` identifier of a sender. And is the `SAME` as 
    the value of `sender`. This is weird that they have different names, but it is how `RASA` designed it.
    
    - `text`: similarly to the previous, the text should be shown to the user. It should be shown BEFORE 
    showing the actual buttons.
    
    - `buttons` is a `list` of `dictionaries`. 
    Each of those dictionaries have SOLELY the following structure:
        - `title`: and this holds the text we want to put as a `title` of a button.
        - `payload`: this holds a string that we want to send to the chatbot, if the user clicks on that 
        particular button. 
        - For example, in above example, if the user clicks on button with the title `Yes!` we want the 
        local program to send text message with the message: `/affirm`; i.e. we want the local program to send 
        the following json to chatbot: ```{'sender': 42,  'message': '/affirm'}```
    
### (2.4) DICTIONARY HOLDING THE 'CUSTOM DICTIONARY WITH SCRIPTS

- The example of the dictionary holding custom with scripts and probabilities:

```Python
{'recipient_id': '42', 
'custom': {'scrips_and_probabilities': ['IpConfigRenew', 0.9686018228530884]}}
```

- This is the dictionary, when the chatbot wants to execute concrete script from the script database.

- The dictionary has the following structure:
    - `recipient_id` is a `string` representing the `UNIQUE` identifier of a sender. And is the `SAME` as 
    the value of `sender`. This is weird that they have different names, but it is how `RASA` designed it.
    
    - `custom` is key, under which we have a dictionary with the following keys and values:
        - `scrips_and_probabilities` is a key under which we have a `list`. The list has only 2 members.
        The first member of that list is a name of the script we want to execute on user's computer. The
        second member of that list is the probability of that script.

### (2.5) DICTIONARY HOLDING THE 'BUTTONS' DICTIONARY BUT MEANT AS 'SURVEY' (yet to be implemented in chatbot)

- The example of the dictionary holding buttons, but is meant as a survey:

```Python
{
"recipient_id": 1024,
"text": "Please rate the conversation.",
"buttons": [{'title': 'Satisfied', 'payload': '/satisfied{}'}, 
            {'title': 'Neutral.', 'payload': '/neutral{}'}, 
            {'title': 'Dissatisfied', 'payload': '/dissatisfied'}]
}
```

- This is a dictionary holding the survey, that will be shown only after the user finishes the conversation.

- The dictionary has the following structure:

    - `recipient_id` is a `string` representing the `UNIQUE` identifier of a sender. And is the `SAME` as 
    the value of `sender`. This is weird that they have different names, but it is how `RASA` designed it.
    
    - `text`; the same logic as before 
    
    - `buttons`; pretty much the same logic as above
    
    - now this might be a different than the normal button dictionary, because for example you might 
    want to show emoticons instead of the titled buttons, i.e. you might want to 
    map: `Satisfied` -> `:)`; `Neutral` -> `:|` and `Dissatisfied` -> `:(`
